#+TITLE: Configuration

#+AUTHOR: Maciej Barć
#+DATE: <2022-07-09 Sat 15:54>
#+LANGUAGE: en

#+LATEX_CLASS: article
#+OPTIONS: num:t toc:t

#+STARTUP: showall inlineimages


* Emacs

  #+BEGIN_SRC yaml :mkdirp yes :tangle playbooks/emacs.yml
    ---
    - hosts: base
      roles:
        - emacs
  #+END_SRC

** Main

   #+BEGIN_SRC yaml :mkdirp yes :tangle playbooks/roles/emacs/tasks/main.yml
     ---
     - include_tasks: install.yml
   #+END_SRC

** Install

   #+BEGIN_SRC yaml :mkdirp yes :tangle playbooks/roles/emacs/tasks/install.yml
     ---
     - name: Install GNU Emacs
       tags: [development, editor, emacs, install, lisp, system]
       become: true
       become_method: su
       become_user: root
       block:
         - name: Install GNU Emacs on Gentoo
           when: ansible_distribution == "Gentoo"
           community.general.portage:
             state: present
             package:
               - app-admin/emacs-updater
               - app-editors/emacs
               - app-eselect/eselect-emacs
   #+END_SRC
