ANSIBLE             := ansible
ANSIBLE-LINT        := $(ANSIBLE)-lint
ANSIBLE-PLAYBOOK    := $(ANSIBLE)-playbook
EMACS               := emacs
PYTHON              := python3
RM                  := rm
RMDIR               := $(RM) -r

EMACS-FLAGS         := --batch -q
PLAY-FLAGS          := -vv --ask-become-pass

PWD                 ?= $(shell pwd)
DOCS                := $(PWD)/docs
SCRIPTS             := $(PWD)/scripts
SRC                 := $(PWD)/src
INVENTORY           := $(SRC)/inventory
PLAYBOOKS           := $(SRC)/playbooks

ANSIBLE-CFG         := $(SRC)/ansible.cfg
EMACS-REQUIREMENTS  := $(PWD)/requirements.el
PYTHON-REQUIREMENTS := $(PWD)/requirements.txt


.PHONY: all
all: clean tangle


.PHONY: clean-cfg
clean-cfg:
	if [ -f $(ANSIBLE-CFG) ] ; then $(RM) $(ANSIBLE-CFG) ; fi

.PHONY: clean-docs
clean-docs:
	if [ -d $(DOCS) ] ; then $(RMDIR) $(DOCS) ; fi

.PHONY: clean-inventory
clean-inventory:
	if [ -d $(INVENTORY) ] ; then $(RMDIR) $(INVENTORY) ; fi

.PHONY: clean-playbooks
clean-playbooks:
	if [ -d $(PLAYBOOKS) ] ; then $(RMDIR) $(PLAYBOOKS) ; fi

.PHONY: clean
clean: clean-cfg clean-docs clean-inventory clean-playbooks


.PHONY: emacs-requirements
emacs-requirements:
	$(EMACS) $(EMACS-FLAGS) --script $(EMACS-REQUIREMENTS)

.PHONY: python-requirements
python-requirements:
	$(PYTHON) -m pip install --user --verbose -r $(PYTHON-REQUIREMENTS)

.PHONY: requirements
requirements: emacs-requirements python-requirements


tangle-%:
	$(EMACS) $(EMACS-FLAGS) $(SRC)/$(*).org -f org-babel-tangle

.PHONY: tangle
tangle: tangle-Configuration
tangle: tangle-Cron tangle-Desktop tangle-Emacs tangle-Git
tangle: tangle-Inventory tangle-MyDot tangle-Scheme


play-%:
	$(ANSIBLE-PLAYBOOK) $(PLAY-FLAGS) $(PLAYBOOKS)/$(*).yml

.PHONY: play
play: play-cron play-desktop play-emacs
play: play-git play-mydot play-scheme


lint-%:
	$(ANSIBLE-LINT) $(PLAYBOOKS)/$(*).yml

.PHONY: lint
lint: lint-cron lint-desktop lint-emacs
lint: lint-git lint-mydot lint-scheme


.PHONY: test
test: lint


docs:
	mkdir -p $(DOCS)

.PHONY: index
index:
	$(PYTHON) $(SCRIPTS)/index.py

html-%: docs
	$(EMACS) $(EMACS-FLAGS) $(SRC)/$(*).org -f org-html-export-to-html
	mv $(SRC)/$(*).html $(DOCS)/$(*).html
	$(MAKE) index

.PHONY: html
html: html-Configuration html-Desktop html-Emacs html-Inventory
html: html-MyDot html-Scheme

pdf-%: docs
	$(EMACS) $(EMACS-FLAGS) $(SRC)/$(*).org -f org-latex-export-to-pdf
	mv $(SRC)/$(*).pdf $(DOCS)/$(*).pdf
	mv $(SRC)/$(*).tex $(DOCS)/$(*).tex
	$(MAKE) index

.PHONY: pdf
pdf: pdf-Configuration pdf-Desktop pdf-Emacs pdf-Git pdf-Inventory
pdf: pdf-MyDot pdf-Scheme

.PHONY: doc
doc: html pdf
