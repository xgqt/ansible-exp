#!/usr/bin/env python3


"""
Generate "index.html" in "docs" directory.
"""


import os


def generate_index_contents():
    """
    Generate contents of the "index.html" file.
    """

    index_contents = "<html>"
    index_contents += "<head>"
    index_contents += "</head>"
    index_contents += "<body>"
    index_contents += "<ul>"

    file_count = 0

    for found_file in sorted(os.listdir(".")):

        if found_file == "index.html":
            continue

        index_contents += f"<li><a href=\"{found_file}\">"
        index_contents += found_file
        index_contents += "</a></li>"
        index_contents += "\n"

        file_count += 1

    index_contents += "</ul>"
    index_contents += "</body>"
    index_contents += "</html>"

    return index_contents, file_count


def main():
    """
    Main.
    """

    if not os.path.exists("./docs"):
        raise RuntimeError("Documentation directory does not exist!")

    os.chdir("./docs")
    pwd = os.path.abspath(os.getcwd())

    with open("index.html", "w", encoding="utf-8") as index_file:
        index_contents, file_count = generate_index_contents()

        index_file.write(index_contents)

        print(f"Indexed {file_count} files from \"{pwd}\".")


if __name__ == "__main__":
    main()
