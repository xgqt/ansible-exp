;;; requirements.el --- Install required packages -*- lexical-binding: t -*-



;;; Commentary:


;; Install required packages.



;;; Code:


(require 'package)


(cond
 ((equal (require 'htmlize nil t) 'htmlize)
  (message "Package ‘htmlize’ already installed."))
 (t
  (message "Package ‘htmlize’ not yet installed, installing it now.")
  (package-initialize)
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-refresh-contents)
  (package-install 'htmlize)))


;;; requirements.el ends here
